<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Daftar Dosen</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Latest compiled and minified CSS -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

</head>

<body>
    <!-- start of topbar -->
    <nav class="navbar fixed-top navbar-expand-sm navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img src="assets/Group 2.png" alt="SI-RG" width="30" height="30" class="d-inline-block align-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleMobileMenu" aria-controls="toggleMobileMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleMobileMenu">

            <ul class="navbar-nav ms-auto text-center">
                <li>
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li>
                    <a class="nav-link" href="#">Tawaran Judul PA</a>
                </li>
                <li>
                    <a class="nav-link" href="#">Karya</a>
                </li>
            </ul>

        </div>
    </nav>
    <!-- end of top bar -->

    <!-- start of main content -->
    <section>

        <div class="awal">
            <img src="Pens-Campus.jpeg" class="img-fluid" alt="ini foto">
        </div>
        <div class="list-dosen">
            <div class="container pt-5">
                <div class="row">
                    <div class="card my-2">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="Pens-Campus.jpeg" class="card-img">
                            </div>
                            <div class="col-md-6">
                                <div class="card-detail text-start">
                                    <h2>Nama</h2>
                                    <p>Umi Sa'adah, S.Kom, M.Kom</p>
                                    <h2>NIP</h2>
                                    <p>197404162000032003</p>
                                    <h2>Tahun Begabung</h2>
                                    <p>2022</p>
                                    <h2>No Telepon</h2>
                                    <p>0821</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- end of main content -->


    <!-- start of footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-col">
                    <img src="assets/image 3.png" class="img-footer">
                    <div class="company-name">
                        <p><b>DEPARTEMEN TEKNIK INFORMATIKA<br>DAN KOMPUTER POLITEKNIK<br>ELEKTRONIKA NEGERI SURABAYA</b></p>
                    </div>
                    <div class="company-address">
                        <p>Jl. Raya ITS, Sukolilo, Kota Surabaya, 60111</p>
                    </div>
                    <div class="company-contact">
                        <p><b>Phone</b> : 098<br><b>Email</b> : @pens.ac.id</p>
                    </div>
                </div>
                <div class="footer-col">
                    <form class="responsive-footer-form">
                        <div class="form-wrapper">
                            <div class="form">
                                <div class="form-item">
                                    <h4>Pesan</h4>
                                    <div class="footer-input">
                                        <input type="text" />
                                    </div>
                                    <textarea class="fill-vertical-space"></textarea>
                                </div>
                                <div class="kirim-pesan">
                                    <a href="#" class="button-kirim-pesan"><b>KIRIM PESAN</b></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="footer-col">
                    <h4>Media Sosial</h4>
                    <div class="social-links">
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fa-brands fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- end of footer -->


    <!-- start of script -->
    <script src="js/bootstrap.min.js"></script>

</body>

<!-- end of script -->

</html>


<style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Roboto', sans-serif;
    }

    nav {
        padding: 50px;
    }

    .nav-link {
        font-family: 'Roboto', sans-serif;
        font-weight: bold;
    }

    /* .navbar-brand {
  padding-left: 100px;
} */


    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Roboto', sans-serif;
    }

    .container {
        max-width: 1170px;
        margin: auto;
    }

    .row {
        display: flex;
        flex-wrap: wrap;
    }

    .footer {
        background-color: rgb(20, 72, 122);
        padding: 70px 0;
    }

    .footer-col {
        width: 25%;
        padding: 0 15px;
    }

    .company-name {
        padding-top: 20px;
    }

    .company-name p {
        color: #ffffff;
        font-size: 14px;
    }

    .company-address p {
        color: #ffffff;
        font-size: 14px;
    }

    .company-contact p {
        color: #ffffff;
        font-size: 14px;
    }

    .button-kirim-pesan {
        background-color: #F3C903;
        border: none;
        color: #14487A;
        padding: 5px 25px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }

    .footer-col h4 {
        font-size: 18px;
        color: #F3C903;
        text-transform: capitalize;
        margin-bottom: 35px;
        font-weight: 500;
        position: relative;
    }

    .footer-col .social-links a {
        display: inline-block;
        height: 40px;
        width: 40px;
        background-color: rgba(255, 255, 255, 0.2);
        margin: 0 10px 10px 0;
        text-align: center;
        line-height: 40px;
        border-radius: 50%;
        color: #ffffff;
        transition: all 0.5s ease;
    }

    .footer-input {
        padding-bottom: 20px;
    }

    .footer-col .social-links a:hover {
        color: #24262b;
        background-color: #ffffff;
    }

    .form-wrapper {
        display: flex;
        justify-content: space-between;
        align-items: flex-start;
        flex-wrap: wrap;
        position: relative;
    }

    .form {
        width: 100%;
    }

    .form-item {
        margin-bottom: 1em;
    }

    label {
        margin-bottom: 0.5em;
        display: block;
        box-sizing: border-box;
        width: 100%;
    }

    .footer-col input {
        display: block;
        box-sizing: border-box;
        width: 100%;
    }

    .footer-col textarea {
        display: block;
        box-sizing: border-box;
        width: 100%;
        border: 0;
        height: 100px;
        resize: vertical;
    }

    /*responsive*/
    @media(max-width: 767px) {
        .footer-col {
            width: 50%;
            margin-bottom: 30px;
        }
    }

    @media(max-width: 574px) {
        .footer-col {
            width: 100%;
        }
    }
</style>